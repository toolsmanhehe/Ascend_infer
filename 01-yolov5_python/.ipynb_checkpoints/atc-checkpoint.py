import os
import yaml


def yaml_load(yaml_file=''):
    with open(yaml_file, errors='ignore') as f:
        return yaml.safe_load(f)


def main():
    config_file = 'config.yaml'
    config_yaml = yaml_load(config_file).get('detection')
    atc_path = config_yaml['atc_path']
    weights = config_yaml['model']
    soc = config_yaml['soc']
    infer_type = config_yaml['input_shape']
    framework = config_yaml['framework']
    input_format = config_yaml['input_format']
    out_path = config_yaml['output']
    fp16_nodes = config_yaml['input_fp16_nodes']

    cmd = f'bash {atc_path} --input_shape={infer_type} --input_fp16_nodes={fp16_nodes} --input_format={input_format} --output={out_path} --soc_version={soc} --framework={framework} --model={weights}'

    print(cmd)
    os.system(cmd)


if __name__ == "__main__":
    main()