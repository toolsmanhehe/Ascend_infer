# Ascend_infer
本仓库为昇腾Atlas 200i DK A2开发者套件的推理代码，部分案例已适配Atlas 200 Dk。
本仓库案例包含了推理的基本流程，以及推理的各种模式，包括单次推理、多次推理、异步推理、同步推理、多模型推理、多输入推理、多输出推理、多输入多输出推理、多个模型多个输入多个输出推理等。同时，本仓库也包含了推理的各种优化，包括推理的多线程优化、推理的多流优化、推理的混合精度优化、推理的离线模型转换优化等。

# 案例说明
## 01-yolov5_python
本案例是基于python语言的yolov5推理案例，提供从.onnx模型到.om模型的转换步骤，以及图片预处理、推理、后处理的完整代码。支持图片、视频、摄像头输入。本案例已经成功移植至昇腾Atlas 200 DK开发者套件。

如需使用昇腾Atlas 200i DK A2开发者套件进行推理
- 更换模型为`yolo.om`或重新使用`best.onnx`模型进行转换
- 在图片预处理步骤中将`img = np.ascontiguousarray(img, dtype=np.float16)/255.0`修改为`img = np.ascontiguousarray(img, dtype=np.float16)`

具体使用方法请参考案例中的README.md。

以下案例尚未测试在昇腾Atlas 200 DK开发者套件上的可用性，如需使用请自行测试。
## 02-ocr

## 03-resnet

## 04-image-HDR-enhance

## 05-cartoonGAN_picture

## 06-human_protein_map_classification

## 07-Unet++

## 08-portrait_pictures

## 09-speech-recognition

